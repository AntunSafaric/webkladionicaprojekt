CREATE DATABASE  IF NOT EXISTS `kladionica` /*!40100 DEFAULT CHARACTER SET cp1250 */;
USE `kladionica`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: localhost    Database: kladionica
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `odabir`
--

DROP TABLE IF EXISTS `odabir`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `odabir` (
  `IDKorisnika` varchar(45) NOT NULL,
  `IDUtakmice` int(11) NOT NULL,
  `odabir` varchar(45) DEFAULT NULL,
  KEY `idutakmice_idx` (`IDUtakmice`),
  CONSTRAINT `idutakmice` FOREIGN KEY (`IDUtakmice`) REFERENCES `utakmica` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1250;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `odabir`
--

LOCK TABLES `odabir` WRITE;
/*!40000 ALTER TABLE `odabir` DISABLE KEYS */;
/*!40000 ALTER TABLE `odabir` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utakmica`
--

DROP TABLE IF EXISTS `utakmica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utakmica` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `domacin` varchar(45) NOT NULL,
  `gost` varchar(45) NOT NULL,
  `datumUtakmice` date NOT NULL,
  `rezultat` varchar(45) DEFAULT NULL,
  `odabir` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=654 DEFAULT CHARSET=cp1250;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utakmica`
--

LOCK TABLES `utakmica` WRITE;
/*!40000 ALTER TABLE `utakmica` DISABLE KEYS */;
INSERT INTO `utakmica` VALUES (1,'Španjolska','Turska','2016-06-17',NULL,NULL),(2,'Češka','Hrvatska  ','2016-06-17',NULL,NULL),(3,'Italija','Švedska  ','2016-06-17',NULL,NULL),(4,'Belgija','Irska','2016-06-18',NULL,NULL),(5,'Portugal','Austrija','2016-06-18',NULL,NULL),(6,'Island','Mađarska','2016-06-18',NULL,NULL);
/*!40000 ALTER TABLE `utakmica` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-17  4:00:16
