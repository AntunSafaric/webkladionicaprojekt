﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebKladionica.Helpers
{
    public class KladionicaHelper
    {
        public static string PretvoriRezultat(string rezultat)
        {
            string kraj = "";

            if (rezultat == null)
            {
                return "Nedefiniran";

            }
            else
            {


                string[] lista = rezultat.Split(':');
                int domacin = Convert.ToInt32(lista[0]);
                int gost = Convert.ToInt32(lista[1]);
                if (domacin > gost)
                {
                    kraj = "1";
                }
                else if (domacin < gost)
                {
                    kraj = "2";
                }
                else kraj = "X";
            }
            

            return kraj;
        }
    }
}