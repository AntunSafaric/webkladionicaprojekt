﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using WebKladionica.Models;

namespace WebKladionica.Reports
{
    public class UtakmiceReport
    {
        public byte[] Podaci { get; private set; }

        public UtakmiceReport(List<Utakmice> utakmice)
        {
            // kreiranje pdf dokumenta sa velicinom stranice i marginama
            // mjere su aproksimativno points (1pt = 1/72 incha)
            Document pdfDokument = new Document(PageSize.A4, 50, 50, 20, 50);

            // na disku
            // // kreiranje datoteke sa jedinstvenim nazivom
            // string nazivDokumenta = Guid.NewGuid() + ".pdf";
            // // dokumenti se spremaju u mapu PDFs
            // string path = Path.Combine(HostingEnvironment.MapPath("~/Reports/Pdfs"), nazivDokumenta);
            // // kreiranje pdf dokumenta na disku
            // PdfWriter.GetInstance(pdfDokument, new FileStream(path, FileMode.Create));

            // u memoriji
            MemoryStream memStream = new MemoryStream();
            PdfWriter.GetInstance(pdfDokument, memStream).CloseStream = false;

            // otvaranje dokumenta
            pdfDokument.Open();

            // definiranje fontova
            BaseFont font = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, false /*embedded*/);
            // Font header = FontFactory.GetFont("Times", 10, Font.BOLD); // alternativno
            Font header = new Font(font, 12, Font.NORMAL, BaseColor.DARK_GRAY);
            Font naslov = new Font(font, 14, Font.BOLDITALIC, BaseColor.BLACK);
            Font tekst = new Font(font, 10, Font.NORMAL, BaseColor.BLACK);

            var logo = iTextSharp.text.Image.GetInstance(HostingEnvironment.MapPath("~/Content/Slike/Logo.png"));
            // logo.SetAbsolutePosition(440, 800); // apsolutno poziciniranje
            logo.Alignment = Element.ALIGN_LEFT; // relativno pozicioniranje
            logo.ScaleAbsoluteWidth(250); // resize
            logo.ScaleAbsoluteHeight(100);
            pdfDokument.Add(logo);

            // header - tekst generiramo pomocu objekata Chunk, Phrase i Paragraph
            Paragraph p = new Paragraph("PAUP projekt", header);
            pdfDokument.Add(p);

            // naslov
            p = new Paragraph("POPIS UTAKMICA", naslov);
            p.Alignment = Element.ALIGN_CENTER;
            p.SpacingBefore = 30;
            p.SpacingAfter = 30;
            pdfDokument.Add(p);

            // tablica
            PdfPTable t = new PdfPTable(4); // 5 kolona
            t.WidthPercentage = 100; // širina tablice
            t.SetWidths(new float[] { 2, 3, 3, 5 }); // relativni odnosi sirina kolona

            // definiranje zaglavlja tablice
            // dodajemo kolone
            t.AddCell(VratiCeliju("Broj utakmice", tekst, BaseColor.LIGHT_GRAY, true));
            t.AddCell(VratiCeliju("Domacin", tekst, BaseColor.LIGHT_GRAY, true));
            t.AddCell(VratiCeliju("Gost", tekst, BaseColor.LIGHT_GRAY, true));
            t.AddCell(VratiCeliju("Datum utakmice", tekst, BaseColor.LIGHT_GRAY, true));

            // dodajemo redove
            foreach (Utakmice u in utakmice)
            {
                t.AddCell(VratiCeliju(u.Id.ToString(), tekst, BaseColor.WHITE, false));
                t.AddCell(VratiCeliju(u.domacin, tekst, BaseColor.WHITE, false));
                t.AddCell(VratiCeliju(u.gost, tekst, BaseColor.WHITE, false));
                t.AddCell(VratiCeliju(u.datumUtakmice.ToShortDateString(), tekst, BaseColor.WHITE, false));

            }

            // dodajemo tablicu na dokument
            pdfDokument.Add(t);

            // mjesto i vrijeme
            p = new Paragraph("Čakovec, " + System.DateTime.Now.ToString("dd.MM.yyyy"), header);
            p.Alignment = Element.ALIGN_RIGHT;
            p.SpacingBefore = 30;
            pdfDokument.Add(p);

            //zatvarnaje dokumenta
            pdfDokument.Close();
            Podaci = memStream.ToArray();
        }

        // metoda vraća jednu čeliju
        private PdfPCell VratiCeliju(string labela, Font font, BaseColor boja, bool wrap)
        {
            PdfPCell c1 = new PdfPCell(new Phrase(labela, font));
            c1.BackgroundColor = boja;
            c1.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
            c1.Padding = 5;
            c1.NoWrap = wrap;
            return c1;
        }
    }
}