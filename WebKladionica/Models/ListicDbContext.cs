﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebKladionica.Models
{
    public class ListicDbContext: DbContext
    {
        public DbSet<Utakmice> Utakmica{ get; set; }
        public DbSet<Odabir> Odabiri { get; set; }
      
    }
}