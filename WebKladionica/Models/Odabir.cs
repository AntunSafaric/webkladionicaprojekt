﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebKladionica.Models
{
    [Table("odabir")]
    public class Odabir
    {
       
      
        [Column("IDUtakmice")]
        [Key]
        public int IDUtakmice { get; set; }
        [ForeignKey("IDUtakmice")]
        public virtual Utakmice utakmica { get; set; }
        [Column("IDKorisnika")]
        public string IDKorisnika { get; set; }
       
        [Column("odabir")]

        public string odabir { get; set; }

       
    }
}