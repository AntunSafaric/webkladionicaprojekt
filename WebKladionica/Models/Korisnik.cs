﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebKladionica.Models
{
    [Table("korisnik")]
    public class Korisnik
    {
        //klasa za podatke za login
        [Key]
        public int UserID { get; set; }

        [Required(ErrorMessage = "Obavezno je upisati ime.")]
        public string Ime { get; set; }

        [Required(ErrorMessage = "Obavezno je upisati prezime.")]
        public string Prezime { get; set; }

        [Required(ErrorMessage = "Obavezno je upisati korisničko ime!")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Obavezno je upisati Email adresu.")]
        [EmailAddress(ErrorMessage = "Email adresa nije ispravna!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Obavezno je upisati lozinku!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Lozinka i ponovljena lozinka nisu jednake!")]
        [DataType(DataType.Password)]
        public string PotvrdiPassword { get; set; }
    }
}