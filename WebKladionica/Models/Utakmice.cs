﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;


namespace WebKladionica.Models
{
    [Table("utakmica")]
    public class Utakmice
    {
        //POCO
        //trenutačno služi samo za testiranje(10.05.2016)

        [Display(Name = "ID")]
        [Key]
        public int Id { get; set; }

        [Column("domacin")]
        [Display(Name = "Domaćin")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} je obavezan podatak")]
        public string domacin { get; set; }

        [Column("gost")]
        [Display(Name = "Gost")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "{0} je obavezan podatak")]
        public string gost { get; set; }

        [Column("datumUtakmice")]
        [Display(Name = "Datum događaja")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Datum utakmice  je obavezan podatak")]
        [DataType(DataType.Date)]
        public DateTime datumUtakmice { get; set; }       

        [Column("rezultat")]
        [Display(Name = "Rezultat utakmice")]
        [RegularExpression(@"(?<!-)[0-9]+:[0-9]+(?!-)",ErrorMessage ="Format za rezultat je neispravan!")]
        public string rezultat { get; set; }

        [Column("odabir")]
        [Display(Name = "Odabir")]
        public string odabir { get; set; }



        

       


       






    }
}