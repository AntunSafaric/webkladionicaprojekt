﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebKladionica.Models;

namespace WebKladionica.Controllers
{
    public class KorisnikController : Controller
    {
        public ActionResult Index()
        {
            using (ListicDbContext baza = new ListicDbContext())
            {
                return View(baza.Korisnik.ToList());
            }
        }

        //metoda za registraciju
        public ActionResult Registracija()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registracija(Korisnik account)
        {
            if (ModelState.IsValid)
            {
                using (ListicDbContext baza = new ListicDbContext())
                {
                    baza.Korisnik.Add(account);
                    baza.SaveChanges();
                }
                ModelState.Clear();
                ViewBag.Message = account.Ime + " " + account.Prezime + " -registracija uspješno obavljena.";
            }
            return View();
        }

        //metoda za login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Korisnik user)
        {
            using (ListicDbContext baza = new ListicDbContext())
            {
                var usr = baza.Korisnik.SingleOrDefault(u => u.Username == user.Username && u.Password == user.Password);
                if (usr != null)
                {
                    Session["UserID"] = usr.UserID.ToString();
                    Session["Username"] = usr.Username.ToString();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Username ili password je krivi.");
                }
            }
            return View();
        }

        //metoda za logedin page
        public ActionResult Loggedin()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
    }
}