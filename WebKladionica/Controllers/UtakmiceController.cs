﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebKladionica.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using WebKladionica.Reports;
using System.IO;

namespace WebKladionica.Controllers
{
    [Authorize]
    public class UtakmiceController : Controller
    {

        private ListicDbContext utakmiceDb = new ListicDbContext();
        // GET: Utakmice
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Detalji()
        {
            return View();
        }
        [Authorize]
        public ActionResult Utakmice(string domacin)
        {
            var popis = from s in utakmiceDb.Utakmica select s;

            if (!String.IsNullOrEmpty(domacin))
                popis = popis.Where(st => (st.domacin + " " + st.gost).ToUpper().Contains(domacin.ToUpper()));

            ViewBag.Title = "Popis utakmica";
            return View(popis);
        }
        [Authorize]
        public ActionResult Azuriraj(int? id)
        {
            Utakmice u1;
            if (id == null)
            {                
                u1 = new Utakmice();
                ViewBag.Title = "Upis nove utakmice";
            }
            else
            {               
                u1 = utakmiceDb.Utakmica.Find(id);
                if (u1 == null)
                {
                    return HttpNotFound();
                }
                ViewBag.Title = "Ažuriranje podataka o utakmici";
            }           
            return View(u1);
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Azuriraj(
           [Bind(Include = "Id,domacin,gost,datumUtakmice,rezultat,odabir")] Utakmice u1 )
        {      
            if (u1.datumUtakmice < DateTime.Now)
                ModelState.AddModelError("datumUtakmice", "Datum utakmice treba biti jednak ili veći od današnjeg datuma");
            // provjera da li je model prošao validaciju
            if (ModelState.IsValid)
            {
                if (u1.Id != 0)
                {                    
                    // ažuriranje postojećeg 
                   
                    utakmiceDb.Entry(u1).State = System.Data.Entity.EntityState.Modified;
                }               
                else
                {
                    // upis novog
                   
                    utakmiceDb.Utakmica.Add(u1);    
                }
                utakmiceDb.SaveChanges();

                return RedirectToAction("Utakmice");
            }
            utakmiceDb.SaveChanges();
            ViewBag.Title = "Ažuriranje podataka o utakmici";
            return View(u1);
        }

        //brisanje utakmice
        // GET metoda
        [Authorize(Roles="Admin")]
        public ActionResult Obrisi(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Utakmice u1 = utakmiceDb.Utakmica.Find(id);
            if (u1 == null)
            {
                return HttpNotFound();
            }
            ViewBag.Title = "Brisanje studenta";
            return View(u1);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Obrisi")]
        [ValidateAntiForgeryToken]
        public ActionResult ObrisiPotvrda(int id)
        {
            Utakmice u1 = utakmiceDb.Utakmica.Find(id);
            utakmiceDb.Utakmica.Remove(u1);
            utakmiceDb.SaveChanges();
            return RedirectToAction("Utakmice");
        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        //metoda slična kao azuriraj, ali je potrebna za autorizaciju korisnika
        public ActionResult _odaberiOdabirPartial([Bind(Include = "Id,domacin,gost,datumUtakmice,odabir")] Utakmice u1)
        {
            Odabir o1 = new Odabir();    
            o1.IDKorisnika = User.Identity.GetUserId();
            o1.odabir = u1.odabir;
            int broj = u1.Id;
            if (u1.datumUtakmice < DateTime.Now)
                ModelState.AddModelError("datumUtakmice", "Datum utakmice treba biti veći od današnjeg datuma");
            Odabir o;
            // provjera da li je model prošao validaciju
            if (ModelState.IsValid)
            {
                if (u1.Id != 0)
                {
                    o1.IDUtakmice = broj;
                    o = utakmiceDb.Odabiri.Where(x => x.IDUtakmice == broj && x.IDKorisnika == o1.IDKorisnika).SingleOrDefault();
                    if (o == null)
                    {
                        utakmiceDb.Odabiri.Add(o1);
                    }
                    else
                    {
                        o.odabir = u1.odabir;
                        utakmiceDb.SaveChanges();
                    }                
                    // ažuriranje postojećeg                    
                    utakmiceDb.Entry(u1).State = System.Data.Entity.EntityState.Modified;
                }
                else
                {
                    // upis novog
                    utakmiceDb.Utakmica.Add(u1);
                }               
                utakmiceDb.SaveChanges();
                return RedirectToAction("Utakmice");
            }

            utakmiceDb.SaveChanges();
            ViewBag.Title = "Ažuriranje podataka o utakmici";
            return PartialView(u1);
        }
        [Authorize]
        public PartialViewResult _odaberiOdabirPartial(int? id )
        {
            Utakmice u1;

            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);            
                u1 = new Utakmice();
                ViewBag.Title = "Upis nove utakmice";
            }
            else
            {
               
                u1 = utakmiceDb.Utakmica.Find(id);
                if (u1 == null)
                {
                    return null;
                }
                ViewBag.Title = "Ažuriranje podataka o utakmici";
            }
           
            return PartialView(u1);
        }
        [Authorize]
        public ActionResult PopisZaKorisnika()
        {
           
            var korisnik = User.Identity.GetUserId();


            var popis = from utk in utakmiceDb.Utakmica join odb in utakmiceDb.Odabiri 
                        on utk.Id equals odb.IDUtakmice where odb.IDKorisnika==korisnik select utk;
                        
            ViewBag.Title = "Popis utakmica";

            return View(popis);
            
        }

        public FileStreamResult Ispisi(string gost, string domacin)
        {
            // EF - lista sa filtriranjem
            var popis = from s in utakmiceDb.Utakmica select s;
            // filtriranja

            if (!String.IsNullOrEmpty(gost))
            {
                popis = popis.Where(m => (m.gost).ToUpper().Contains(gost.ToUpper()));
            }

            if (!String.IsNullOrEmpty(domacin))
            {
                popis = popis.Where(m => (m.domacin).ToUpper().Contains(domacin.ToUpper()));
            }        

            UtakmiceReport report = new UtakmiceReport(popis.ToList());
            return new FileStreamResult(new MemoryStream(report.Podaci), "application/pdf");

        
        }




    }
}