﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebKladionica.Startup))]
namespace WebKladionica
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
